/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.settings

import android.Manifest
import android.app.Application
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.telecom.TelecomManager
import androidx.activity.result.ActivityResultCallback
import androidx.compose.material3.SnackbarHostState
import androidx.core.content.ContextCompat
import androidx.core.content.getSystemService
import androidx.lifecycle.*
import de.wivewa.android.R
import de.wivewa.android.database.AppDatabase
import de.wivewa.android.database.Database
import de.wivewa.android.database.model.ConfigurationItem
import de.wivewa.android.database.model.Customer
import de.wivewa.android.database.model.PhoneAccountUploadConfiguration
import de.wivewa.android.extensions.withModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consume
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.invoke
import kotlinx.coroutines.launch

class PhoneModel(application: Application): AndroidViewModel(application) {
    companion object {
        private val UPLOAD_ROLES = listOf("pbx.upload")
    }

    sealed class ActivityCommand {
        object RequestPhoneStateAndPhoneNumbersPermission: ActivityCommand()
        class RequestDialerIntegrationPermission(val intent: Intent): ActivityCommand()
    }

    sealed class ApiState {
        object UnsupportedDialer: ApiState()
        object ConnectionError: ApiState()
        class MissingPermission(val requestPermission: () -> Unit): ApiState()
        object Ready: ApiState()
    }

    sealed class CallerInfoState {
        sealed class Disabled: CallerInfoState() {
            object ApiNotReady: Disabled()
            class CanEnable(val enable: () -> Unit): Disabled()
        }

        class Enabled(
            val disable: () -> Unit,
            val hasIssue: Boolean
        ): CallerInfoState()
    }

    data class CallUploadState(
        val accounts: List<Account>,
        val issue: Issue?,
        val edit: EditAccount
    ) {
        sealed class Issue {
            object ApiNotReady: Issue()
            class MissingOsPermission(val requestPermission: () -> Unit): Issue()
            class LastSyncFailed(val exception: String): Issue()
        }

        data class Account(
            val osId: String,
            val componentName: ComponentName,
            val label: String,
            val customer: Customer?,
            val issue: Issue?
        ) {
            data class Customer(val id: Long, val name: String)

            sealed class Issue {
                object LostUploadPermission: Issue()
                object LostPhoneAccount: Issue()
            }
        }

        sealed class EditAccount {
            class None(val start: (Account) -> Unit): EditAccount()
            class Some(
                val account: Account,
                val customers: List<Customer>,
                val cancel: () -> Unit,
                val update: (Customer?) -> Unit
            ): EditAccount()
        }
    }

    private val database: Database = AppDatabase.with(application)
    private val getSystemIntegrationState: () -> Flow<PhoneIntegrationStatus.State> = {
        PhoneIntegrationStatus.getFrom(getApplication())
    }
    private val telecomManager = application.getSystemService<TelecomManager>()!!

    val snackbarHostState = SnackbarHostState()

    private val activityCommandChannelInternal = Channel<ActivityCommand>()
    val activityCommand: ReceiveChannel<ActivityCommand> = activityCommandChannelInternal

    val phoneStateAndNumbersCallback = ActivityResultCallback<Map<String, Boolean>> { retryGetPhoneAccounts() }

    private val retryGetSystemIntegrationState = MutableStateFlow(0)
    fun retryGetSystemIntegrationState() { retryGetSystemIntegrationState.update { it + 1 } }
    val systemIntegrationState = retryGetSystemIntegrationState.flatMapLatest {
        getSystemIntegrationState().map { state ->
            when (state) {
                PhoneIntegrationStatus.State.UnsupportedDialer -> ApiState.UnsupportedDialer
                PhoneIntegrationStatus.State.ConnectionError -> ApiState.ConnectionError
                is PhoneIntegrationStatus.State.MissingPermission -> ApiState.MissingPermission {
                    activityCommandChannelInternal.trySend(
                        ActivityCommand.RequestDialerIntegrationPermission(state.requestPermissionIntent)
                    )
                }
                PhoneIntegrationStatus.State.Ready -> ApiState.Ready
            }
        }
    }.withModel(this)

    private val isSystemIntegrationReady = systemIntegrationState.map { it is ApiState.Ready }
    private val isCallerInfoEnabled = database.configuration
        .getValueLive(ConfigurationItem.PHONE_CALLER_INFO)
        .map { it == ConfigurationItem.Bool.YES }

    val callerInfoState: Flow<CallerInfoState> =
        combine(isSystemIntegrationReady, isCallerInfoEnabled) { a, b -> Pair(a, b) }
            .transformLatest { (isSystemIntegrationReady, isCallerInfoEnabled) ->
                if (!isCallerInfoEnabled) {
                    if (isSystemIntegrationReady) {
                        val enableChannel = Channel<Unit>()

                        emit(CallerInfoState.Disabled.CanEnable(
                            enable = { enableChannel.trySend(Unit) }
                        ))

                        enableChannel.consume {
                            for (attempt in enableChannel) {
                                Dispatchers.IO.invoke {
                                    database.configuration.setValue(
                                        ConfigurationItem.PHONE_CALLER_INFO,
                                        ConfigurationItem.Bool.YES
                                    )
                                }
                            }
                        }
                    } else emit(CallerInfoState.Disabled.ApiNotReady)
                } else {
                    val disableChannel = Channel<Unit>()

                    emit(CallerInfoState.Enabled(
                        disable = { disableChannel.trySend(Unit) },
                        hasIssue = !isSystemIntegrationReady
                    ))

                    disableChannel.consume {
                        for (attempt in disableChannel) {
                            Dispatchers.IO.invoke {
                                database.configuration.setValue(
                                    ConfigurationItem.PHONE_CALLER_INFO,
                                    ConfigurationItem.Bool.NO
                                )
                            }
                        }
                    }
                }
            }.withModel(this)

    private val retryGetPhoneAccounts = MutableStateFlow(0)
    private fun retryGetPhoneAccounts() { retryGetPhoneAccounts.update { it + 1 } }
    private val phoneAccounts = retryGetPhoneAccounts.map {
        try {
            if (ContextCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) throw SecurityException()

            telecomManager.callCapablePhoneAccounts.map { telecomManager.getPhoneAccount(it) }
        } catch (ex: SecurityException) {
            null
        }
    }
    private val customersWithCallLogUploadPermission: Flow<List<Customer>> = database.customer.selectAllWithRoleLive(UPLOAD_ROLES)
    private val phoneUploadConfigurationItems = database.phoneAccountUploadConfiguration.getLiveWithCustomerNameAndPermission(UPLOAD_ROLES)
    private val currentPhoneException = database.configuration.getValueLive(ConfigurationItem.PHONE_EXCEPTION)
    private val currentlyEditedAccountLive = MutableStateFlow(null as Pair<ComponentName, String>?)
    val callUploadState = combine(
        combine(
            phoneAccounts, customersWithCallLogUploadPermission, phoneUploadConfigurationItems
        ) { a, b, c -> Triple(a, b, c) },
        currentPhoneException, isSystemIntegrationReady, currentlyEditedAccountLive
    ) { bundle, currentPhoneException, isSystemIntegrationReady, currentlyEditedAccount ->
        val (phoneAccounts, customersWithCallLogUploadPermission, phoneUploadConfigurationItems) = bundle

        val nativeAccountsById = phoneAccounts?.associateBy { Pair(it.accountHandle.componentName, it.accountHandle.id) } ?: emptyMap()
        val databaseAccountsById = phoneUploadConfigurationItems.associateBy { item -> item.phoneAccountUploadConfiguration.let { Pair(it.componentName, it.osHandleId) } }

        val accountsCurrentlyReported = nativeAccountsById.map { nativeAccountMapItem ->
            val (id, nativeAccount) = nativeAccountMapItem

            val databaseAccount = databaseAccountsById[id]

            val issue =
                if (databaseAccount?.hasPermission == false) CallUploadState.Account.Issue.LostUploadPermission
                else null

            CallUploadState.Account(
                osId = id.second,
                componentName = id.first,
                label = nativeAccount.label.toString(),
                customer = databaseAccount?.let {
                    CallUploadState.Account.Customer(
                        databaseAccount.phoneAccountUploadConfiguration.uploadEntityId,
                        databaseAccount.customerName
                    )
                },
                issue = issue
            )
        }

        val accountsConfiguredButNotReported = (databaseAccountsById - nativeAccountsById.keys).values.map { databaseItem ->
            val issue =
                if (!databaseItem.hasPermission) CallUploadState.Account.Issue.LostUploadPermission
                else CallUploadState.Account.Issue.LostPhoneAccount

            CallUploadState.Account(
                osId = databaseItem.phoneAccountUploadConfiguration.osHandleId,
                componentName = databaseItem.phoneAccountUploadConfiguration.componentName,
                label = databaseItem.phoneAccountUploadConfiguration.label,
                customer = CallUploadState.Account.Customer(
                    databaseItem.phoneAccountUploadConfiguration.uploadEntityId,
                    databaseItem.customerName
                ),
                issue = issue
            )
        }

        val accounts = accountsCurrentlyReported + accountsConfiguredButNotReported

        val issue = if (!isSystemIntegrationReady) CallUploadState.Issue.ApiNotReady
        else if (phoneAccounts == null) CallUploadState.Issue.MissingOsPermission {
            activityCommandChannelInternal.trySend(ActivityCommand.RequestPhoneStateAndPhoneNumbersPermission)
        }
        else if (currentPhoneException != null) CallUploadState.Issue.LastSyncFailed(currentPhoneException)
        else null

        val realCurrentlyEditedAccount = accounts.find {
            it.osId == currentlyEditedAccount?.second && it.componentName == currentlyEditedAccount.first
        }

        if (currentlyEditedAccount != null && realCurrentlyEditedAccount == null) {
            currentlyEditedAccountLive.compareAndSet(currentlyEditedAccount, null)
        }

        val edit: CallUploadState.EditAccount = if (realCurrentlyEditedAccount == null)
            CallUploadState.EditAccount.None {
                currentlyEditedAccountLive.compareAndSet(
                    currentlyEditedAccount,
                    Pair(it.componentName, it.osId)
                )
            }
        else
            CallUploadState.EditAccount.Some(
                account = realCurrentlyEditedAccount,
                customers = customersWithCallLogUploadPermission,
                cancel = {
                    currentlyEditedAccountLive.compareAndSet(currentlyEditedAccount, null)
                },
                update = { newCustomer ->
                    if (currentlyEditedAccountLive.compareAndSet(currentlyEditedAccount, null)) {
                        viewModelScope.launch {
                            try {
                                Dispatchers.IO.invoke {
                                    database.runInTransaction {
                                        if (newCustomer == null) {
                                            database.phoneAccountUploadConfiguration.delete(
                                                realCurrentlyEditedAccount.osId,
                                                realCurrentlyEditedAccount.componentName
                                            )
                                        } else {
                                            val existing = database.phoneAccountUploadConfiguration.getByOsParams(
                                                realCurrentlyEditedAccount.osId,
                                                realCurrentlyEditedAccount.componentName
                                            )

                                            if (existing == null) {
                                                database.phoneAccountUploadConfiguration.insert(
                                                    PhoneAccountUploadConfiguration(
                                                        id = 0,
                                                        componentName = realCurrentlyEditedAccount.componentName,
                                                        osHandleId = realCurrentlyEditedAccount.osId,
                                                        label = realCurrentlyEditedAccount.label,
                                                        uploadEntityId = newCustomer.id
                                                    )
                                                )
                                            } else {
                                                database.phoneAccountUploadConfiguration.update(
                                                    existing.copy(uploadEntityId = newCustomer.id)
                                                )
                                            }
                                        }

                                        database.loggedPhoneCall.removeForUnusedCustomerIds()
                                    }
                                }
                            } catch (ex: Exception) {
                                launch {
                                    snackbarHostState.showSnackbar(
                                        getApplication<Application>().getString(R.string.generic_error)
                                    )
                                }
                            }
                        }
                    }
                }
            )

        CallUploadState(
            accounts = accounts,
            issue = issue,
            edit = edit
        )
    }.withModel(this)
}