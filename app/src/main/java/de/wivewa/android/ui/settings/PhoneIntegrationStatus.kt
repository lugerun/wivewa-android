/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.ui.settings

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import de.wivewa.android.integration.dialer.client.SimpleDialerClient
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.suspendCancellableCoroutine

object PhoneIntegrationStatus {
    private val handler = Handler(Looper.getMainLooper())

    sealed class State {
        object UnsupportedDialer: State()
        object ConnectionError: State()
        class MissingPermission(val requestPermissionIntent: Intent): State()
        object Ready: State()
    }

    fun getFrom(context: Context): Flow<State> = flow {
        val client = SimpleDialerClient(context, handler)

        try {
            client.start()

            val hasPermission = suspendCancellableCoroutine { result ->
                client.checkPermissionGranted { result.resumeWith(it) }
            }

            if (hasPermission) {
                emit(State.Ready)
            } else {
                emit(State.MissingPermission(client.requestPermissionIntent()))
            }
        } catch (ex: SimpleDialerClient.UnsupportedDialerException) {
            emit(State.UnsupportedDialer)
        } catch (ex: Exception) {
            emit(State.ConnectionError)
        } finally {
            client.stop()
        }
    }
}