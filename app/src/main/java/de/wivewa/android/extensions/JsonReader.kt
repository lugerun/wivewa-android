/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.extensions

import android.util.JsonReader
import android.util.JsonToken

fun JsonReader.nullableString(): String? = when (peek()) {
    JsonToken.STRING -> nextString()
    JsonToken.NULL -> { nextNull(); null }
    else -> throw IllegalStateException()
}

fun JsonReader.nullableLong(): Long? = when (peek()) {
    JsonToken.NUMBER -> nextLong()
    JsonToken.NULL -> { nextNull(); null }
    else -> throw IllegalStateException()
}

fun <T> JsonReader.readList(readItem: (JsonReader) -> T): List<T> = mutableListOf<T>().also { list ->
    beginArray()
    while (hasNext()) list.add(readItem(this))
    endArray()
}