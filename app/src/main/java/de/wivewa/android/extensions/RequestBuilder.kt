/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.extensions

import android.util.JsonWriter
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.Request
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.OutputStreamWriter

fun Request.Builder.postJson(serialize: (writer: JsonWriter) -> Unit): Request.Builder =
    this.post(object: RequestBody() {
        override fun contentType(): MediaType = "application/json; charset=utf-8".toMediaType()
        override fun writeTo(sink: BufferedSink) {
            JsonWriter(
                OutputStreamWriter(
                    sink.buffer.outputStream()
                )
            ).use { writer ->
                serialize(writer)
            }
        }
    })