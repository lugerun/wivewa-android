/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.network

import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.webkit.ClientCertRequest
import java.security.KeyPairGenerator
import java.security.KeyStore
import java.security.KeyStore.PrivateKeyEntry
import java.security.MessageDigest
import java.security.cert.X509Certificate
import java.security.spec.ECGenParameterSpec
import java.util.*
import javax.security.auth.x500.X500Principal

object DeviceAuthentication {
    private const val KEYSTORE = "AndroidKeyStore"
    private const val KEY_NAME = "wivewa-client-certificate"

    fun getPrivateKeyEntry(): PrivateKeyEntry {
        synchronized(this) {
            val keyStore = KeyStore.getInstance(KEYSTORE).also { it.load(null) }

            keyStore.getEntry(KEY_NAME, null)?.also {
                if (it is PrivateKeyEntry) return it
                else throw ClassCastException()
            }

            val now = System.currentTimeMillis()

            KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, KEYSTORE)
                .also {
                    it.initialize(
                        KeyGenParameterSpec.Builder(
                            KEY_NAME,
                            KeyProperties.PURPOSE_SIGN
                        )
                            .setAlgorithmParameterSpec(
                                ECGenParameterSpec("prime256v1")
                            )
                            .setDigests(
                                KeyProperties.DIGEST_NONE,
                                KeyProperties.DIGEST_SHA256,
                                KeyProperties.DIGEST_SHA384,
                                KeyProperties.DIGEST_SHA512
                            )
                            .setCertificateSubject(X500Principal("", mapOf(
                                "C" to "DE",
                                "O" to "wivewa",
                                "CN" to Build.MODEL
                            )))
                            .setCertificateNotBefore(Date(now - 1000 * 60 * 60 * 24))
                            .setCertificateNotAfter(Date(now + 1000L * 60 * 60 * 24 * 365 * 10))
                            .build()
                    )
                }.genKeyPair()

            keyStore.getEntry(KEY_NAME, null).also {
                if (it is PrivateKeyEntry) return it
                else throw IllegalStateException()
            }
        }
    }

    fun getCertificateSha256() = MessageDigest.getInstance("SHA-256")
        .digest(getPrivateKeyEntry().certificate.encoded)
        .map { it.toUByte().toString(16).padStart(2, '0') }
        .joinToString(separator = "")

    fun getClientKeyManager() = getPrivateKeyEntry().let { entry ->
        X509ClientKeyManager(entry.privateKey, entry.certificate as X509Certificate)
    }

    fun handle(request: ClientCertRequest) {
        val entry = getPrivateKeyEntry()

        request.proceed(
            entry.privateKey,
            arrayOf(entry.certificate as X509Certificate)
        )
    }
}