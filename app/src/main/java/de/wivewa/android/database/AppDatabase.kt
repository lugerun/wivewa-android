/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database

import android.content.Context
import androidx.room.Room
import de.wivewa.android.database.migration.Migrations

object AppDatabase {
    private const val DATABASE_NAME = "wivewa"

    private var instance: Database? = null

    fun with(context: Context) = instance ?: synchronized(this) {
        instance ?: Room.databaseBuilder(
            context,
            Database::class.java,
            DATABASE_NAME
        ).addMigrations(*Migrations.ALL).build().also { instance = it }
    }
}