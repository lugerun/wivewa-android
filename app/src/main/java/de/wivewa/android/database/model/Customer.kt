/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model

import android.util.JsonReader
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "customer")
data class Customer (
    @PrimaryKey
    val id: Long,
    val name: String
) {
    companion object {
        private const val ID = "id"
        private const val NAME = "name"

        fun fromJson(reader: JsonReader): Customer {
            var id: Long? = null
            var name: String? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ID -> id = reader.nextLong()
                    NAME -> name = reader.nextString()
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return Customer(
                id = id!!,
                name = name!!
            )
        }
    }
}