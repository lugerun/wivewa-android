/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "configuration")
data class ConfigurationItem (
    @PrimaryKey
    val key: String,
    val value: String
) {
    object Bool {
        const val NO = "no"
        const val YES = "yes"
    }

    companion object {
        const val DOMAIN = "domain"
        const val PHONE_EXCEPTION = "phone_exception"
        const val PHONE_CALLER_INFO = "phone_caller_info"
        const val ADDRESSBOOK_LAST_SYNC_TIMESTAMP = "addressbook_last_sync_timestamp"
    }
}