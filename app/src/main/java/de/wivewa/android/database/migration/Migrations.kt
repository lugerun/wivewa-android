/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.migration

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object Migrations {
    private val V3 = object: Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("DROP TABLE logged_phone_call")

            database.execSQL("CREATE TABLE IF NOT EXISTS `logged_phone_call` (`id` TEXT NOT NULL, `kind` TEXT NOT NULL, `from` TEXT NOT NULL, `to` TEXT NOT NULL, `start` INTEGER NOT NULL, `wait` INTEGER NOT NULL, `length` INTEGER NOT NULL, `status` TEXT NOT NULL, `synced` INTEGER NOT NULL, `entity_id` INTEGER NOT NULL, PRIMARY KEY(`id`), FOREIGN KEY(`entity_id`) REFERENCES `customer`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )")
            database.execSQL("CREATE INDEX IF NOT EXISTS `phone_call_sync_index` ON `logged_phone_call` (`synced`)")
            database.execSQL("CREATE INDEX IF NOT EXISTS `phone_call_status_index` ON `logged_phone_call` (`status`)")
            database.execSQL("CREATE INDEX IF NOT EXISTS `index_logged_phone_call_entity_id` ON `logged_phone_call` (`entity_id`)")

            database.execSQL("CREATE TABLE IF NOT EXISTS `phone_account_upload_configuration` (`id` INTEGER NOT NULL, `component_name` TEXT NOT NULL, `os_handle_id` TEXT NOT NULL, `upload_entity_id` INTEGER NOT NULL, PRIMARY KEY(`id`), FOREIGN KEY(`upload_entity_id`) REFERENCES `customer`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )")
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS `phone_account_upload_unique_os_handle` ON `phone_account_upload_configuration` (`component_name`, `os_handle_id`)")
            database.execSQL("CREATE INDEX IF NOT EXISTS `index_phone_account_upload_configuration_upload_entity_id` ON `phone_account_upload_configuration` (`upload_entity_id`)")
        }
    }

    private val V4 = object: Migration(3, 4) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("DROP TABLE `phone_account_upload_configuration`")

            database.execSQL("CREATE TABLE IF NOT EXISTS `phone_account_upload_configuration` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `component_name` TEXT NOT NULL, `os_handle_id` TEXT NOT NULL, `upload_entity_id` INTEGER NOT NULL, FOREIGN KEY(`upload_entity_id`) REFERENCES `customer`(`id`) ON UPDATE CASCADE ON DELETE CASCADE )")
            database.execSQL("CREATE UNIQUE INDEX IF NOT EXISTS `phone_account_upload_unique_os_handle` ON `phone_account_upload_configuration` (`component_name`, `os_handle_id`)")
            database.execSQL("CREATE INDEX IF NOT EXISTS `index_phone_account_upload_configuration_upload_entity_id` ON `phone_account_upload_configuration` (`upload_entity_id`)")
        }
    }

    val ALL = arrayOf(V3, V4)
}