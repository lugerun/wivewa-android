/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model.join

import androidx.room.ColumnInfo

data class AddressWithTypeInfo (
    @ColumnInfo(name = "other_entity_id")
    val otherEntityId: Long?,
    @ColumnInfo(name = "contact_type_name")
    val contactTypeName: String,
    val additional: String,
    val street: String,
    val city: String,
    val country: String,
    @ColumnInfo(name = "zip_code")
    val zipCode: String
) {
    @Transient
    val printable =
        listOf(street, additional, "$zipCode $city", country)
            .map { it.trim() }
            .filter { it.isNotEmpty() }
            .joinToString(separator = "\n")
}