/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model

import android.content.ComponentName
import androidx.room.ColumnInfo
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@androidx.room.Entity(
    tableName = "phone_account_upload_configuration",
    indices = [
        Index(
            name = "phone_account_upload_unique_os_handle",
            value = ["component_name", "os_handle_id"],
            unique = true
        )
    ],
    foreignKeys = [
        ForeignKey(
            entity = Customer::class,
            parentColumns = ["id"],
            childColumns = ["upload_entity_id"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class PhoneAccountUploadConfiguration(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    @ColumnInfo(name = "component_name")
    val componentName: ComponentName,
    @ColumnInfo(name = "os_handle_id")
    val osHandleId: String,
    @ColumnInfo(defaultValue = "")
    val label: String,
    @ColumnInfo(name = "upload_entity_id", index = true)
    val uploadEntityId: Long
)
