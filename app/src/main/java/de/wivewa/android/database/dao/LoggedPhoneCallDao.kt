/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import de.wivewa.android.database.model.LoggedPhoneCall

@Dao
interface LoggedPhoneCallDao {
    @Insert
    fun insert(call: LoggedPhoneCall)

    @Update
    fun update(call: LoggedPhoneCall)

    @Query("SELECT * FROM logged_phone_call WHERE id = :id")
    fun findById(id: String): LoggedPhoneCall?

    @Query("SELECT * FROM logged_phone_call WHERE status IN (:status)")
    fun findByStatus(status: Set<String>): List<LoggedPhoneCall>

    @Query("SELECT * FROM logged_phone_call WHERE synced = 0")
    fun findAllPending(): List<LoggedPhoneCall>

    @Delete
    fun remove(call: LoggedPhoneCall)

    @Query("DELETE FROM logged_phone_call WHERE entity_id NOT IN (SELECT upload_entity_id FROM phone_account_upload_configuration)")
    fun removeForUnusedCustomerIds()

    @Query("DELETE FROM logged_phone_call")
    fun removeAll()
}