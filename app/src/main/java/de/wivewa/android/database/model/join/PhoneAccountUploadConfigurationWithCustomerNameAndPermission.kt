/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model.join

import androidx.room.ColumnInfo
import androidx.room.Embedded
import de.wivewa.android.database.model.PhoneAccountUploadConfiguration

data class PhoneAccountUploadConfigurationWithCustomerNameAndPermission(
    @Embedded
    val phoneAccountUploadConfiguration: PhoneAccountUploadConfiguration,
    @ColumnInfo(name = "customer_name")
    val customerName: String,
    @ColumnInfo(name = "customer_has_permission")
    val hasPermission: Boolean
)