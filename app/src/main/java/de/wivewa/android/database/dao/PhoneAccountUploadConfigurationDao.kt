/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.dao

import android.content.ComponentName
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import de.wivewa.android.database.model.PhoneAccountUploadConfiguration
import de.wivewa.android.database.model.join.PhoneAccountUploadConfigurationWithCustomerNameAndPermission
import kotlinx.coroutines.flow.Flow

@Dao
interface PhoneAccountUploadConfigurationDao {
    @Query("SELECT phone_account_upload_configuration.*, customer.name AS customer_name, CASE WHEN EXISTS (SELECT * FROM customer_role WHERE customer_role.role IN (:roles) AND customer_role.customer_id = customer.id) THEN 1 ELSE 0 END as customer_has_permission FROM phone_account_upload_configuration JOIN customer ON (phone_account_upload_configuration.upload_entity_id = customer.id)")
    fun getLiveWithCustomerNameAndPermission(roles: List<String>): Flow<List<PhoneAccountUploadConfigurationWithCustomerNameAndPermission>>

    @Query("DELETE FROM phone_account_upload_configuration WHERE os_handle_id = :osId AND component_name = :componentName")
    fun delete(osId: String, componentName: ComponentName)

    @Query("SELECT * FROM phone_account_upload_configuration WHERE os_handle_id = :osId AND component_name = :componentName")
    fun getByOsParams(osId: String, componentName: ComponentName): PhoneAccountUploadConfiguration?

    @Insert
    fun insert(item: PhoneAccountUploadConfiguration)

    @Update
    fun update(item: PhoneAccountUploadConfiguration)
}