/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.converter

import android.content.ComponentName
import androidx.room.TypeConverter

class ComponentNameConverter {
    @TypeConverter
    fun componentNameToString(componentName: ComponentName?) = componentName?.flattenToString()

    @TypeConverter
    fun stringToComponentName(componentName: String?) = componentName?.let { ComponentName.unflattenFromString(it) }
}