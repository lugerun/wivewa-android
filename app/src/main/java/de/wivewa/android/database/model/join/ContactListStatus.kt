/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.model.join

import androidx.room.ColumnInfo

data class ContactListStatus (
    @ColumnInfo(name = "last_sync")
    val lastSync: Long?,
    @ColumnInfo(name = "has_customers")
    val hasCustomers: Boolean,
    @ColumnInfo(name = "has_contacts")
    val hasContacts: Boolean
)