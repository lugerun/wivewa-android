/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import de.wivewa.android.database.model.ConfigurationItem
import kotlinx.coroutines.flow.Flow

@Dao
interface ConfigurationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertItem(item: ConfigurationItem)

    fun setValue(key: String, value: String) = upsertItem(ConfigurationItem(key, value))

    @Query("SELECT value FROM configuration WHERE `key` = :key")
    fun getValue(key: String): String?

    @Query("SELECT value FROM configuration WHERE `key` = :key")
    fun getValueLive(key: String): Flow<String?>

    @Query("DELETE FROM configuration WHERE `key` = :key")
    fun removeValue(key: String)
}