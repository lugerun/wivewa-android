/*
 * wivewa-android - the Android client for the WiVeWa CRM
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.sync

import de.wivewa.android.database.Database
import de.wivewa.android.database.model.ConfigurationItem
import de.wivewa.android.database.model.PhoneCallStatusStrings
import de.wivewa.android.network.ServerApiFactory
import de.wivewa.android.network.UploadPhoneCall
import kotlinx.coroutines.*
import java.util.concurrent.Callable

object PhoneCallSync {
    suspend fun execute(apiFactory: ServerApiFactory, database: Database) {
        val (domain, pendingCalls) = Dispatchers.IO.invoke {
            database.runInTransaction(Callable {
                val domain = database.configuration.getValue(ConfigurationItem.DOMAIN)
                val pendingCalls = database.loggedPhoneCall.findAllPending()

                Pair(domain, pendingCalls)
            })
        }

        if (domain == null) return
        if (pendingCalls.isEmpty()) return

        val api = apiFactory.create(domain)

        val pendingCallsByEntity = pendingCalls.groupBy { it.entityId }

        supervisorScope {
            val tasks = pendingCallsByEntity.map { mapItem ->
                val (customerId, calls) = mapItem

                async {
                    api.uploadPhoneCalls(
                        customerId,
                        calls.map { call ->
                            UploadPhoneCall(
                                id = call.id,
                                kind = call.kind,
                                from = call.from,
                                to = call.to,
                                start = call.start,
                                wait = call.wait,
                                length = call.length,
                                status = call.status
                            )
                        }
                    )

                    Dispatchers.IO.invoke {
                        database.runInTransaction {
                            for (oldLoggedCall in calls) {
                                val currentLoggedCall =
                                    database.loggedPhoneCall.findById(oldLoggedCall.id)

                                if (currentLoggedCall != oldLoggedCall) continue

                                if (PhoneCallStatusStrings.TYPE_DONE.contains(currentLoggedCall.status)) {
                                    database.loggedPhoneCall.remove(currentLoggedCall)
                                } else {
                                    database.loggedPhoneCall.update(currentLoggedCall.copy(synced = true))
                                }
                            }
                        }
                    }
                }
            }

            tasks.joinAll()
            tasks.awaitAll()
        }
    }
}